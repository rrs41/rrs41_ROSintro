
from __future__ import print_function
from six.moves import input
#import all libraries
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
import math
#define ROS libraries
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

#Get and move to home position
joint_goal1 = [-0.018141789571106237, -1.670119277803801, 2.1534367089438433, -0.47906628218570724, -0.018345732792008462, -0.08267840911439439]
move_group.go(joint_goal1, wait=True)
move_group.stop()

#Draw the R 
poses = []
poses.append([0, 0.18])
poses.append([0, 0.68])
for i in range(11): #parametric ellipse
	poses.append([0.2 * math.sin(i/10.0 * math.pi), 0.125 * math.cos(i/10.0 * math.pi) + 0.555])

poses.append([0.2, 0.18])

#DRAW THE I
poses.append([-0.2,0.18])
poses.append([0, 0.18])
poses.append([0, 0.68])
poses.append([-0.2, 0.68])
poses.append([0.2, 0.68])
poses.append([0, 0.68])

#DRAW THE S using two ellipses
for i in range(11):
	poses.append([-0.2 * math.sin(i/10.0 * math.pi), 0.125 * math.cos(i/10.0 * math.pi) + 0.555])

for i in range(11):
	poses.append([0.2 * math.sin(i/10.0 * math.pi), 0.125 * math.cos(i/10.0 * math.pi) + 0.305])

#function to move to a set pose
def moveToPose(pose):
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.position.x = 0.22777571069668023
    pose_goal.position.y = pose_i[0]
    pose_goal.position.z = pose_i[1]
    pose_goal.orientation.x = 0.7064944178927286
    pose_goal.orientation.y = 0.7066399048230647
    pose_goal.orientation.z = 0.027641773834441996
    pose_goal.orientation.w = 0.027597368304788462

    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()

for pose_i in poses:
	#pose = move_group.get_current_pose().pose
	pose_goal = geometry_msgs.msg.Pose()
	pose_goal.position.x = 0.22777571069668023
	pose_goal.position.y = pose_i[0]
	pose_goal.position.z = pose_i[1]
	pose_goal.orientation.x = 0.7064944178927286
	pose_goal.orientation.y = 0.7066399048230647
	pose_goal.orientation.z = 0.027641773834441996
	pose_goal.orientation.w = 0.027597368304788462

	move_group.set_pose_target(pose_goal)
	success = move_group.go(wait=True)
	move_group.stop()

