# !/ usr / bin / bash
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, -3]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[-1.0, 3.2, 0.0]' '[0.0,0.0,0.0]'
rosservice call /turtle1/set_pen 255 0 0 1 1
rosservice call /turtle1/teleport_absolute 8 5 0
rosservice call /turtle1/set_pen 255 0 0 1 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[0, 3, 0]' '[0,0,0]'
rosservice call /turtle1/set_pen 255 0 0 1 1
rosservice call /turtle1/teleport_absolute 8 9 0
rosservice call /turtle1/set_pen 255 0 0 1 0
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
-- '[3,0,0]' '[0,0,8]'
rosservice call /turtle1/set_pen 255 0 0 1 1
rosservice call /turtle1/teleport_absolute 0 0 0
